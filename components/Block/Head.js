import Head from "next/head";

// eslint-disable-next-line react/prop-types
export default ({ title }) => (
  <Head>
    <meta charSet="utf-8" />
    <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="author" content="The Travel Agent Next Door" />
    <title>{title}</title>
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap"
      rel="stylesheet"
      type="text/css"
    />
    <link href="../../css/bootstrap.min.css" rel="stylesheet" />
  </Head>
);
