const nextRoutes = require("next-routes");

// eslint-disable-next-line no-multi-assign
const routes = (module.exports = nextRoutes());

routes
  .add("about", "/about-us")
  .add("index", "/")
  // --- apollo testing ---
  .add("space", "/space")
  .add("launch", "/launch/:slug/:id")
  .add("characters", "/characters")
  .add("character", "/character/:slug/:id");
// --- apollo testing ---

// First argument of add function is page, second route and third is optional
