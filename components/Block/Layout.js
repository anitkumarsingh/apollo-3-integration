import PropTypes from "prop-types";
import Head from "./Head";
import Header from "./Header";

const Layout = ({ children, title }) => {
  return (
    <div>
      <Head title={title} />
      <Header />
      <main>{children}</main>
    </div>
  );
};
Layout.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.any,
  title: PropTypes.string,
};

Layout.defaultProps = {
  children: null,
  title: "",
};

export default Layout;
