import React, { useRef, useState, useEffect } from "react";
import { Link } from "../../routes";

export default function Header() {
  const [isSticky, setSticky] = useState(false);

  const ref = useRef(null);

  const handleScroll = () => {
    if (ref.current) {
      if (window.scrollY > 0) {
        setSticky(true);
      } else {
        setSticky(false);
      }
    }
  };

  useEffect(() => {
    if (window) {
      window.addEventListener("scroll", handleScroll);
    }

    return () => {
      if (window) {
        window.removeEventListener("scroll", () => handleScroll);
      }
    };
  }, []);

  return (
    <>
      <nav
        className={`${
          isSticky
            ? "navbar navbar-expand-lg navbar-light navbar navbar-dark bg-primary header sticky"
            : "navbar navbar-expand-lg navbar-light navbar navbar-dark bg-primary header"
        }`}
        ref={ref}
      >
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarTogglerDemo01"
          aria-controls="navbarTogglerDemo01"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a className="navbar-brand" href="#">
            Logo
          </a>
          <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
            <li className="nav-item active">
              <Link route="index">
                <a className="nav-link">
                  Home <span className="sr-only">(current)</span>
                </a>
              </Link>
            </li>
            <li className="nav-item">
              <Link route="space">
                <a className="nav-link">Space</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link route="characters">
                <a className="nav-link">Characters</a>
              </Link>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">
              Search
            </button>
          </form>
        </div>
      </nav>
      <style jsx>
        {`
          .header {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            padding: 20px 30px;
            z-index: 2;
            border-bottom: 1px solid rgba(255, 255, 255, 0);
          }

          .header .logo_sticky {
            display: none;
          }

          header.sticky {
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            border-bottom: 1px solid #ededed;
            background-color: #fff;
            padding: 15px 20px;
          }
        `}
      </style>
    </>
  );
}
