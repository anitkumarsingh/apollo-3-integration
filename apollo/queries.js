import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const GET_LAUNCH_DETAILS = gql`
  query LaunchDetails($launchId: ID!) {
    launch(id: $launchId) {
      id
      site
      isBooked
      rocket {
        id
        name
        type
      }
      mission {
        name
        missionPatch
      }
    }
  }
`;

export const GET_LAUNCHES = gql`
  query launchList($after: String) {
    launches(after: $after) {
      cursor
      hasMore
      launches {
        id
        isBooked
        rocket {
          id
          name
        }
        mission {
          name
          missionPatch
        }
      }
    }
  }
`;

export const ALL_CHARACTERS = gql`
  query allCharacters {
    characters {
      results {
        id
        name
        image
        gender
      }
    }
  }
`;

export const CHARACTER_QUERY = gql`
  query character($id: ID) {
    results {
      id
      name
      image
      gender
    }
  }
`;
