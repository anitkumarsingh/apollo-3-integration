import React from "react";
import Layout from "../components/Block/Layout";
import { CHARACTER_QUERY } from "../apollo/queries";
import { useQuery } from "@apollo/client";

const Character = ({ id }) => {
  const { data, loading, error } = useQuery(CHARACTER_QUERY, { variables: { id } });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>ERROR: {error.message}</p>;
  if (!data) return <p>Not found</p>;
  return (
    <Layout>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className>{data}</div>
    </Layout>
  );
};

Character.getInitialProps = async function getInitialProps(context) {
  const { id } = context.query;
  return { id };
};
export default Character;
