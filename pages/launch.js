import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import Layout from "../components/Block/Layout";
import Loader from "../components/Common/Loader";
import { GET_LAUNCH_DETAILS } from "../apollo/queries";

const launchDetails = ({ id }) => {
  const launchId = id;

  const { data, loading, error } = useQuery(GET_LAUNCH_DETAILS, { variables: { launchId } });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>ERROR: {error.message}</p>;
  if (!data) return <p>Not found</p>;
  return (
    <Layout>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className="bg_color_1">
        {loading ? (
          <Loader />
        ) : (
          <div className="container margin_60_35 text-center">
            <h3>
              {data.launch.rocket && data.launch.rocket.name} ({data.launch.rocket && data.launch.rocket.type})
            </h3>
            <h5>{data.launch.site}</h5>
            <img
              src={data.launch && data.launch.mission && data.launch.mission.missionPatch}
              alt=""
              width={800}
              height="auto"
            />
          </div>
        )}
      </div>
    </Layout>
  );
};

launchDetails.getInitialProps = async function getInitialProps(context) {
  const { id } = context.query;
  return { id };
};

export default launchDetails;
