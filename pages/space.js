import React from "react";
import { useQuery } from "@apollo/client";
import { Link } from "../routes";
import Slugify from "../utils/slugify";
import { GET_LAUNCHES } from "../apollo/queries";
import { initializeApollo } from "../lib/apollo/apolloClient";
import Layout from "../components/Block/Layout";

const space = () => {
  const { data, loading, error, fetchMore } = useQuery(GET_LAUNCHES);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>ERROR {JSON.stringify(error)}</p>;
  if (!data) return <p>Not found</p>;

  return (
    <Layout>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />

      <div className="container">
        <div className="row">
          {data.launches &&
            data.launches.launches &&
            data.launches.launches.map((launch) => {
              return (
                <div className="grid" key={launch.id}>
                  <div className="card text-center" style={{ width: "33rem", margin: "5px" }}>
                    <Link
                      route="launch"
                      params={{
                        slug: `${Slugify(launch.rocket.name)}`,
                        id: `${launch.id}`,
                      }}
                    >
                      <a>
                        <img
                          src={launch.mission.missionPatch}
                          //   className="img-fluid"
                          alt=""
                          width={500}
                          height="auto"
                        />
                      </a>
                    </Link>

                    <div className="card-body">
                      <h5 className="card-title">{launch.rocket.name}</h5>
                      <p className="card-text">{launch.mission.name}</p>
                      <Link
                        route="launch"
                        params={{
                          slug: `${Slugify(launch.rocket.name)}`,
                          id: `${launch.id}`,
                        }}
                      >
                        <a className="btn btn-primary">View</a>
                      </Link>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>

        {data.launches && data.launches.hasMore && (
          <p className="text-center">
            <button
              onClick={() =>
                fetchMore({
                  variables: {
                    after: data.launches.cursor,
                  },
                  updateQuery: (prev, { fetchMoreResult, ...rest }) => {
                    if (!fetchMoreResult) return prev;
                    return {
                      ...fetchMoreResult,
                      launches: {
                        ...fetchMoreResult.launches,
                        launches: [...prev.launches.launches, ...fetchMoreResult.launches.launches],
                      },
                    };
                  },
                })
              }
              type="button"
              className="btn_1 rounded bg-primary"
            >
              Load More Launches
            </button>
          </p>
        )}
      </div>
    </Layout>
  );
};

export async function getStaticProps() {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: GET_LAUNCHES,
  });

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
  };
}

export default space;
