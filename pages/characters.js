import React from "react";
import { useQuery } from "@apollo/client";
import { ALL_CHARACTERS } from "../apollo/queries";
import Layout from "../components/Block/Layout";
import { Link } from "../routes";
import Slugify from "../utils/slugify";

const Characters = () => {
  const { loading, error, data } = useQuery(ALL_CHARACTERS);
  if (error) return <h1>Error</h1>;
  if (loading) return <p>Loading...</p>;

  return (
    <Layout>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className="container">
        <div className="row">
          {data.characters.results.map((data) => (
            <div className="col-lg-3 col-md-4 col-sm-4 text-center card" key={data.id}>
              <div className="box_grid">
                <Link
                  route="character"
                  params={{
                    slug: `${Slugify(data.name)}`,
                    id: `${data.id}`,
                  }}
                >
                  <figure>
                    <a>
                      <img src={data.image} className="img-fluid" alt="" width={800} height={533} />
                      <div className="read_more">
                        <span>View </span>
                      </div>
                    </a>
                  </figure>
                </Link>

                <div className="wrapper">
                  <Link
                    route="character"
                    params={{
                      slug: `${Slugify(data.name)}`,
                      id: `${data.id}`,
                    }}
                  >
                    <h6>{data.name}</h6>
                  </Link>
                  <p>{data.gender}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <style jsx>
        {`
          .card {
            margin: 1rem;
            flex-basis: 45%;
            padding: 1.5rem;
            text-align: left;
            color: inherit;
            text-decoration: none;
            border: 1px solid #eaeaea;
            border-radius: 10px;
            transition: color 0.15s ease, border-color 0.15s ease;
          }

          .card:hover,
          .card:focus,
          .card:active {
            color: #0070f3;
            border-color: #0070f3;
          }

          .card h3 {
            margin: 0 0 1rem 0;
            font-size: 1.5rem;
          }

          .card p {
            margin: 0;
            font-size: 1.25rem;
            line-height: 1.5;
          }
        `}
      </style>
    </Layout>
  );
};

export default Characters;
